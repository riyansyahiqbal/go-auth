package model

import (
	"time"

	"github.com/google/uuid"
)

type UserSecurity struct {
	ID           uuid.UUID `gorm:"Type:uuid;NOT NULL;PRIMARY_KEY;DEFAULT:uuid_generate_v1()" db="id" json:"id"`
	UserID       uuid.UUID `gorm:"Type:uuid;NOT NULL" db="user_id" json:"user_id"`
	Pwd          []byte    `gorm:"Type:VARCHAR;NOT NULL" db="pwd" json:"pwd"`
	LoginAttempt int16     `gorm:"Type:smallint;NULL" db="login_attempt" json:"login_attempt"`
	CreatedDt    time.Time `gorm:"Type:timestamp with time zone;DEFAULT:CURRENT_TIMESTAMP" db="created_dt" json:"created_dt"`
	CreatedBy    string    `gorm:"Type:VARCHAR;DEFAULT:'SYSTEM'" db="created_by" json:"created_by"`
	ModifiedDt   time.Time `gorm:"Type:timestamp with time zone;DEFAULT:null" db="modified_dt" json:"modified_dt"`
	ModifiedBy   string    `gorm:"Type:smallint;DEFAULT:null" db="modified_by" json:"modified_by"`
}

type UserSecurityDetail struct {
	ID           uuid.UUID `json:"id"`
	UserID       uuid.UUID `json:"user_id"`
	Pwd          []byte    `db="pwd" json:"pwd"`
	LoginAttempt int16     `db="login_attempt" json:"login_attempt"`
	Email        string    `json:"login_attempt"`
	FullName     string    `json:"FullName"`
	PhoneNo      string    `json:"PhoneNo"`
	CreatedDt    time.Time `json:"created_dt"`
	CreatedBy    string    `json:"created_by"`
	ModifiedDt   time.Time `json:"modified_dt"`
	ModifiedBy   string    `json:"modified_by"`
}

func (UserSecurity) TableName() string {
	return "ams_user.t_user_securities"
}
