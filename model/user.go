package model

import (
	"time"

	"github.com/google/uuid"
)

type User struct {
	ID              uuid.UUID `gorm:"Type:uuid;NOT NULL;PRIMARY_KEY;DEFAULT:uuid_generate_v1()" db="id" json:"id"`
	FullName        string    `gorm:"DEFAULT:null" db="full_name" json:"full_name"`
	Email           string    `gorm:"unique" db="email" json:"email"`
	PhoneNo         string    `gorm:"unique" db="phone_no" json:"phone_no"`
	Gender          string    `db="gender" json:"gender"`
	Birthdate       time.Time `gorm:"DEFAULT:null" db="birthdate" json:"birthdate"`
	Address         string    `gorm:"DEFAULT:null" db="address" json:"address"`
	UserDistrictID  uuid.UUID `gorm:"DEFAULT:null" db="user_district_id" json:"district_id"`
	PostalCd        string    `gorm:"DEFAULT:null" db="postal_code" json:"postal_code"`
	AddressAreaName string    `gorm:"DEFAULT:null" db="address_area_name" json:"address_area_name"`
	CreatedDt       time.Time `gorm:"DEFAULT:CURRENT_TIMESTAMP" db="created_dt" json:"created_dt"`
	CreatedBy       string    `gorm:"DEFAULT:'SYSTEM'" db="created_by" json:"created_by"`
	ModifiedDt      time.Time `gorm:"DEFAULT:null" db="modified_dt" json:"modified_dt"`
	ModifiedBy      string    `gorm:"DEFAULT:null" db="modified_by" json:"modified_by"`
}

func (User) TableName() string {
	return "ams_user.t_users"
}
