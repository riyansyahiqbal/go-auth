package param

import "github.com/google/uuid"

type RegisterUserRequest struct {
	FullName        string     `json:"full_name"`
	Email           string     `json:"email"`
	PhoneNo         string     `json:"phone_no"`
	Password        string     `json:"password"`
	Gender          string     `json:"gender"`
	Birthdate       string     `json:"birthdate"`
	Address         string     `json:"address"`
	DistrictID      *uuid.UUID `json:"district_id"`
	PostalCode      string     `json:"postal_code"`
	AddressAreaName string     `json:"address_area_name"`
}

func (request RegisterUserRequest) Validate() error {
	return nil
}

type LoginUserRequest struct {
	CredentialKey      string `json:"credential_key"`
	CredentialPassword string `json:"credential_password"`
}

func (request LoginUserRequest) Validate() error {
	return nil
}

type AuthDetailFilter struct {
	Email   string
	PhoneNo string
}
