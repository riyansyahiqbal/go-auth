package param

type Request interface {
	Validate() error
}
