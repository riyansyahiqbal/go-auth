module auth

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.14
	github.com/spf13/viper v1.7.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
