package repository

import (
	"auth/model"
	"auth/param"
	"fmt"
)

type AuthRepository interface {
	InsertUser(user *model.User) (result *model.User, err error)
	InsertUserSecurity(userSecurity *model.UserSecurity) (result *model.UserSecurity, err error)
	FindUserByCredential(authDetailFiter param.AuthDetailFilter) (result []model.UserSecurityDetail, err error)
}

type authRepository struct {
	db *DataSource
}

func GetAuthRepository(DBTrx *DataSource) AuthRepository {
	var authRepo *authRepository
	if DBTrx != nil {
		authRepo = &authRepository{db: DBTrx}
	} else {
		authRepo = &authRepository{db: DB}
	}
	return authRepo
}

func (repository *authRepository) InsertUser(user *model.User) (result *model.User, err error) {
	result = &model.User{}
	if err := repository.db.Create(user).Scan(result).Error; err != nil {
		return nil, fmt.Errorf("Error while inserting record in database (%s) ", err.Error())
	}
	return result, nil
}

func (repository *authRepository) InsertUserSecurity(userSecurity *model.UserSecurity) (result *model.UserSecurity, err error) {
	result = &model.UserSecurity{}
	if err := repository.db.Create(userSecurity).Scan(result).Error; err != nil {
		return nil, fmt.Errorf("Error while inserting record in database (%s) ", err.Error())
	}
	return result, nil
}

func (repository *authRepository) FindUserByCredential(authDetailFiter param.AuthDetailFilter) (userSecurityDetail []model.UserSecurityDetail, err error) {
	res := repository.db.
		Table("ams_user.t_users").
		Joins("JOIN ams_user.t_user_securities on t_users.id = t_user_securities.user_id").
		Where("t_users.email=?", authDetailFiter.Email).Or("t_users.phone_no=?", authDetailFiter.PhoneNo).
		Select(`t_user_securities.id as id,
				t_user_securities.pwd,
				t_user_securities.user_id,
				t_user_securities.login_attempt,
				t_users.full_name,
				t_users.email,
				t_users.phone_no`).Last(&userSecurityDetail)

	return userSecurityDetail, res.Error
}
