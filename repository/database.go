package repository

import (
	"auth/config"
	"auth/model"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	DB   *DataSource
	once sync.Once
)

type DataSource struct {
	*gorm.DB
}

func GetDataSource(dataSource *DataSource) *DataSource {
	ds := dataSource
	if ds == nil {
		ds = &DataSource{}
	}
	return ds
}

func ConnectDatabase() error {
	once.Do(func() {
		fmt.Println(fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s", config.GetStringValue(config.DB_HOST), config.GetStringValue(config.DB_PORT), config.GetStringValue(config.DB_USER), config.GetStringValue(config.DB_NAME), config.GetStringValue(config.DB_PASSWORD), config.GetStringValue(config.DB_SSL_MODE)))
		conn, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s", config.GetStringValue(config.DB_HOST), config.GetStringValue(config.DB_PORT), config.GetStringValue(config.DB_USER), config.GetStringValue(config.DB_NAME), config.GetStringValue(config.DB_PASSWORD), config.GetStringValue(config.DB_SSL_MODE)))
		if err != nil {
			panic(err.Error())
		}

		maxLifeTime := time.Duration(config.GetIntValue(config.DB_MAX_LIFE_TIME_CONNECTION)) * time.Second
		maxIdleConnection, maxOpenConnection := config.GetIntValue(config.DB_MAX_IDLE_CONNECTION), config.GetIntValue(config.DB_MAX_OPEN_CONNECTION)
		conn.DB().SetMaxOpenConns(maxOpenConnection)
		conn.DB().SetMaxIdleConns(maxIdleConnection)
		conn.DB().SetConnMaxLifetime(maxLifeTime)

		DB = &DataSource{conn}

		fmt.Println("Database connection is established !!!")
	})

	return nil
}

func MigrateDatabase() error {
	if err := DB.Exec(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`).Error; err != nil {
		return errors.New(err.Error())
	}
	DB.AutoMigrate(model.User{}, model.UserSecurity{})
	return nil
}

func BeginTransaction() *DataSource {
	return &DataSource{
		DB: DB.Begin(),
	}
}

func RollbackTransaction(dataSource *DataSource) {
	dataSource.DB.Rollback()
}

func CommitTransaction(dataSource *DataSource) {
	dataSource.DB.Commit()
}
