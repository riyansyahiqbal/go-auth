package main

import (
	"auth/http/server"
	"auth/repository"
	"fmt"
)

func main() {
	err := repository.ConnectDatabase()
	if err != nil {
		fmt.Println(err)
	}

	err = repository.MigrateDatabase()
	if err != nil {
		fmt.Println(err)
	}

	server.StartServer()
}
