package config

const (
	HTTP_SERVER_PORT            = "port"
	CORS_ALLOWED_ORIGINS        = "cors.allowed_origins"
	CORS_ALLOWED_HEADERS        = "cors.allowed_headers"
	CORS_EXPOSED_HEADERS        = "cors.exposed_headers"
	CORS_ALLOWED_METHODS        = "cors.allowed_methods"
	DB_HOST                     = "db.host"
	DB_PORT                     = "db.port"
	DB_USER                     = "db.user"
	DB_PASSWORD                 = "db.password"
	DB_NAME                     = "db.database"
	DB_LOG                      = "db.log"
	DB_SSL_MODE                 = "db.sslMode"
	DB_MAX_OPEN_CONNECTION      = "db.maxOpenConnection"
	DB_MAX_IDLE_CONNECTION      = "db.maxIdleConnection"
	DB_MAX_LIFE_TIME_CONNECTION = "db.maxLifeTimeConnection"
)
