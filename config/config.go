package config

import (
	"fmt"

	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("config")
	viper.AddConfigPath("config/")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

// GetStringValue returns the value associated with the key as a string.
func GetStringValue(key string) (value string) {
	return viper.GetString(key)
}

// GetIntValue returns the value associated with the key as a string.
func GetIntValue(key string) (value int) {
	return viper.GetInt(key)
}

// GetStringSliceValue returns the value associated with the key as a string.
func GetStringSliceValue(key string) (values []string) {
	return viper.GetStringSlice(key)
}

// GetValidatedStringValue returns the value associated with the key as a string agter being validated.
func GetValidatedStringValue(key string) (value string, err error) {
	value = GetStringValue(key)
	if len(value) < 1 {
		return "", fmt.Errorf("Cannot find config for key %s\n", key)
	}

	return value, nil
}
