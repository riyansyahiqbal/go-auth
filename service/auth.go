package service

import (
	"auth/model"
	"auth/param"
	"auth/repository"
	"log"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func RegisterUser(registerRequest *param.RegisterUserRequest) {
	dataSource := repository.BeginTransaction()
	registeredUser := saveUser(dataSource, registerRequest)
	saveUserSecurity(dataSource, registeredUser.ID, registerRequest.Password)
	repository.CommitTransaction(dataSource)
}

func LoginUser(loginRequest *param.LoginUserRequest) *model.UserSecurityDetail {
	authDetailFilter := param.AuthDetailFilter{
		Email:   loginRequest.CredentialKey,
		PhoneNo: loginRequest.CredentialKey,
	}

	userSecurityDetail, err := repository.GetAuthRepository(nil).FindUserByCredential(authDetailFilter)
	if err != nil {
		panic(err)
	}

	err = bcrypt.CompareHashAndPassword(userSecurityDetail[0].Pwd, []byte(loginRequest.CredentialPassword))
	if err != nil {
		log.Fatalf(err.Error())
	}

	return nil
}

func saveUser(dataSource *repository.DataSource, registerRequest *param.RegisterUserRequest) *model.User {
	user := &model.User{
		FullName: registerRequest.FullName,
		Gender:   registerRequest.Gender,
		PhoneNo:  registerRequest.PhoneNo,
		Email:    registerRequest.Email,
	}

	registeredUser, err := repository.GetAuthRepository(dataSource).InsertUser(user)
	if err != nil {
		repository.RollbackTransaction(dataSource)
		panic(err)
	}

	return registeredUser
}

func saveUserSecurity(dataSource *repository.DataSource, userID uuid.UUID, password string) *model.UserSecurity {

	encryptedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		repository.RollbackTransaction(dataSource)
		panic(err)
	}

	userSecurity := &model.UserSecurity{
		UserID: userID,
		Pwd:    encryptedPassword,
	}

	registeredSecurity, err := repository.GetAuthRepository(dataSource).InsertUserSecurity(userSecurity)
	if err != nil {
		repository.RollbackTransaction(dataSource)
		panic(err)
	}

	return registeredSecurity
}
