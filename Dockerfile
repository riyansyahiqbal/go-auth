FROM golang:1.14.4
WORKDIR /go/src/auth
COPY . /go/src/auth
RUN go mod init 
# EXPOSE 8000
CMD ["go","run","/go/src/auth/main.go"]