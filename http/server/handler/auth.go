package handler

import (
	"auth/param"
	"auth/service"
	"fmt"
	"net/http"
)

func Hello(resp http.ResponseWriter, req *http.Request) {
	fmt.Print("Hello World")
}

func RegisterUser(resp http.ResponseWriter, req *http.Request) {
	registerReq := param.RegisterUserRequest{}
	ConvertToJson(&registerReq, req)
	service.RegisterUser(&registerReq)
}

func LoginUser(resp http.ResponseWriter, req *http.Request) {
	loginReq := param.LoginUserRequest{}
	ConvertToJson(&loginReq, req)
	userSecurityDetail := service.LoginUser(&loginReq)
	fmt.Println(userSecurityDetail)
}
