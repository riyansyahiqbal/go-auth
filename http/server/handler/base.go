package handler

import (
	"auth/param"
	"encoding/json"
	"net/http"
)

func ConvertToJson(reqParam param.Request, httpReq *http.Request) error {
	decoder := json.NewDecoder(httpReq.Body)
	if err := decoder.Decode(reqParam); err != nil {
		return err
	}
	return reqParam.Validate()
}
