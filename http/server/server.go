package server

import (
	"auth/config"
	"net/http"
)

func StartServer() {
	http.ListenAndServe(config.GetStringValue(config.HTTP_SERVER_PORT), BuildRouteHandler())
}
