package interceptor

import (
	"net/http"
)

func Before(handlerFunc http.HandlerFunc, command ...string) http.HandlerFunc {
	return func(resp http.ResponseWriter, req *http.Request) {
		// x, err := httputil.DumpRequest(req, true)
		// if err != nil {
		// 	http.Error(resp, fmt.Sprint(err), http.StatusInternalServerError)
		// 	return
		// }
		// log.Println(fmt.Sprintf("%q", x))
		handlerFunc(resp, req)
	}
}
