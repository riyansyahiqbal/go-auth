package server

import (
	"auth/config"
	"auth/http/server/handler"
	"auth/http/server/interceptor"
	"net/http"

	"github.com/gorilla/mux"

	gorillaHandlers "github.com/gorilla/handlers"
)

func BuildRouteHandler() http.Handler {
	router := mux.NewRouter()
	routeUser(router)

	allowedOrigins := config.GetStringSliceValue(config.CORS_ALLOWED_ORIGINS)
	allowedHeaders := config.GetStringSliceValue(config.CORS_ALLOWED_HEADERS)
	exposedHeaders := config.GetStringSliceValue(config.CORS_EXPOSED_HEADERS)
	allowedMethods := config.GetStringSliceValue(config.CORS_ALLOWED_METHODS)
	return gorillaHandlers.CORS(
		gorillaHandlers.AllowedOrigins(allowedOrigins),
		gorillaHandlers.AllowedHeaders(allowedHeaders),
		gorillaHandlers.ExposedHeaders(exposedHeaders),
		gorillaHandlers.AllowedMethods(allowedMethods))(router)
}

func routeUser(router *mux.Router) {
	router.HandleFunc("/hello", interceptor.Before(handler.Hello, "")).Methods(http.MethodGet)
	router.HandleFunc("/users", interceptor.Before(handler.RegisterUser, "")).Methods(http.MethodPost)
	router.HandleFunc("/login", interceptor.Before(handler.LoginUser, "")).Methods(http.MethodPost)
}
